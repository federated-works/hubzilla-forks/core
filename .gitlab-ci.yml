stages:
  - test
  - deploy


# Select what we should cache
cache:
  paths:
  - vendor/
  - .cache/


# global variables for all jobs, if no job specific variables
variables:
  # Tell composer to use the project workspace .cache folder
  COMPOSER_CACHE_DIR: "$CI_PROJECT_DIR/.cache/composer"
  # Ignore a Composer warning
  COMPOSER_ALLOW_SUPERUSER: 1
  # Configure MySQL/MariaDB service (https://hub.docker.com/_/mysql/, https://hub.docker.com/_/mariadb/)
  DB_HOST: mysql
  MYSQL_DATABASE: hello_world_test
  MYSQL_ROOT_PASSWORD: mysql
  # Configure PostgreSQL service (https://hub.docker.com/_/postgres/)
  POSTGRES_DB: ci-db
  POSTGRES_USER: ci-user
  POSTGRES_PASSWORD: ci-pass


before_script:
  # Install & enable Xdebug for code coverage reports
  - apt-get update
  - apt-get install -yqq libicu-dev libjpeg-dev libpng-dev libpq-dev libyaml-dev libzip-dev mariadb-client postgresql-client unzip zip
  - pecl install xdebug yaml
  - docker-php-ext-enable xdebug yaml
  - docker-php-ext-install gd bcmath intl pdo_mysql pdo_pgsql zip

  # Install composer
  - curl -sS https://getcomposer.org/installer | php
  # Install dev libraries from composer
  - php ./composer.phar install --no-progress
  # php.ini settings
  - echo 'xdebug.mode=coverage' >> /usr/local/etc/php/php.ini

# hidden job definition with template for MySQL/MariaDB
.job_template_mysql: &job_definition_mysql
  stage: test
  variables:
    HZ_TEST_DB_HOST: $DB_HOST
    HZ_TEST_DB_TYPE: mysql
    HZ_TEST_DB_USER: root
    HZ_TEST_DB_PASS: $MYSQL_ROOT_PASSWORD
    HZ_TEST_DB_DATABASE: $MYSQL_DATABASE
  script:
    # Import hubzilla's DB schema
    - echo "USE $MYSQL_DATABASE; $(cat ./install/schema_mysql.sql)" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host="$DB_HOST" "$MYSQL_DATABASE"
    # Show databases and relations/tables of hubzilla's database
    - echo "SHOW DATABASES;" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host="$DB_HOST" "$MYSQL_DATABASE"
    - echo "USE $MYSQL_DATABASE; SHOW TABLES;" | mysql --user=root --password="$MYSQL_ROOT_PASSWORD" --host="$DB_HOST" "$MYSQL_DATABASE"
    # Run the actual tests
    - touch dbfail.out
    - vendor/bin/phpunit --configuration tests/phpunit.xml --verbose --stop-on-error --coverage-text --colors=never --log-junit tests/results/junit.xml || exit_code=$?
    - if [ $exit_code -ne 0 ]; then echo "Test barfed!"; cat dbfail.out; exit $exit_code; fi
  coverage: '/^\s*Lines:\s*\d+.\d+\%/'


# hidden job definition with template for PostgreSQL
.job_template_postgres: &job_definition_postgres
  stage: test
  variables:
    HZ_TEST_DB_HOST: postgres
    HZ_TEST_DB_TYPE: postgres
    HZ_TEST_DB_USER: $POSTGRES_USER
    HZ_TEST_DB_PASS: $POSTGRES_PASSWORD
    HZ_TEST_DB_DATABASE: $POSTGRES_DB
  script:
    - export PGPASSWORD=$POSTGRES_PASSWORD
    - psql --version
    - psql -h "postgres" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -c "SELECT VERSION();"
    # Import hubzilla's DB schema
    - psql -h "postgres" -U "$POSTGRES_USER" -v ON_ERROR_STOP=1 --quiet "$POSTGRES_DB" < ./install/schema_postgres.sql
    # Show databases and relations/tables of hubzilla's database
    - psql -h "postgres" -U "$POSTGRES_USER" -l
    - psql -h "postgres" -U "$POSTGRES_USER" -d "$POSTGRES_DB" -c "\dt;"
    # Run the actual tests
    - touch dbfail.out
    - vendor/bin/phpunit --configuration tests/phpunit.xml --verbose --stop-on-error --coverage-text --colors=never --log-junit tests/results/junit.xml || exit_code=$?
    - if [ $exit_code -ne 0 ]; then echo "Test barfed!"; cat dbfail.out; exit $exit_code; fi
  coverage: '/^\s*Lines:\s*\d+.\d+\%/'

# hidden job definition with artifacts config template
.artifacts_template: &artifacts_template
  artifacts:
    expire_in: 1 week
    # Gitlab should show the results, but has problems parsing PHPUnit's junit file.
    reports:
      junit: tests/results/junit.xml
    # Archive test results (coverage, testdox, junit)
    name: "$CI_COMMIT_REF_SLUG-$CI_JOB_NAME"
    paths:
      - tests/results/


# PHP8.1 with MySQL 8.0
php8.1_mysql8.0.22:
  image: php:8.1
  services:
    - mysql:8.0
  <<: *job_definition_mysql
  <<: *artifacts_template

# PHP8.1 with MariaDB 10.6
php8.1_mariadb10.6:
  image: php:8.1
  services:
    - name: mariadb:10.6
      alias: mysql
  <<: *job_definition_mysql
  <<: *artifacts_template

# PHP8.1 with PostgreSQL 12
php8.1_postgres12:
  image: php:8.1
  services:
    - postgres:12-alpine
  <<: *job_definition_postgres
  <<: *artifacts_template


# Generate Doxygen API Documentation and deploy it as GitLab pages
pages:
  stage: deploy
  cache: {}
  image: php:8-cli-alpine
  before_script:
    - apk update
    - apk add doxygen ttf-freefont graphviz
  script:
    - doxygen util/Doxyfile
    - mv doc/html/ public/
    - echo "API documentation should be accessible at https://hubzilla.frama.io/core/ soon"
  artifacts:
    paths:
      - public
  only:
    # Only generate it on main repo's master branch
    - master@hubzilla/core
